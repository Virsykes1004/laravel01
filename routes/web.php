<?php

use App\Http\Controllers\PostController;
use GuzzleHttp\Middleware;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterUserController;
use App\Http\Controllers\AuthenticatedSessionController;

Route::view('/', 'welcome')->name('home');
Route::view('/contact', 'contact')->name('contact');
Route::get('/posts.index',[PostController::class, 'index'])->name('posts.index');
Route::get('/blog/create',[PostController::class, 'create'])->name('posts.create');
Route::post('/blog',[PostController::class, 'store'])->name('posts.store');
Route::get('/blog/{postid}',[PostController::class, 'show'])->name('posts.show');
Route::get('/blog/{postid}/edit',[PostController::class, 'edit'])->name('posts.edit');
Route::patch('/blog/{postid}',[PostController::class,'update'] )->name('posts.update');
Route::delete('/blog/{postid}',[PostController::class,'destroy'])->name('posts.destroy');


Route::view('/about', 'about')->name('about');

Route::get('/login', function(){
    return 'Login page';
})->name('login');

Route::view('/login','auth.login')->name('login');
Route::post('/login', [AuthenticatedSessionController::class, 'store']);
Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');
Route::view('/register','auth.register')->name('register');
Route::post('/register',[RegisterUserController::class,'store']);