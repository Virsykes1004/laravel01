<?php

namespace App\Http\Controllers;

use App\Http\Requests\SavePostRequest;
use App\Models\Post;

use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class PostController extends Controller{

    public function __construct()
    {
        $this->middleware('auth',['except'=>['index','show']]);
    }

    
    public function index()
    {
        $posts= Post::get();

        return view('posts.index', compact('posts'));
    }
    public function show ($postid){
        $posts= Post::findorfail($postid);
         
        return view('posts.show', ['post'=>$posts]);
    }

    public function create(){

        return view('posts.create' ,['posts'=> new Post()]);

    }

    public function store(SavePostRequest $request)
    {
       
        Post::create($request->validate());

        return to_route('posts.index')->with('status','Post created');
        
    }
    public function edit( Post $postid){
        return view('posts.edit',['posts'=> $postid]);
    }
    
    public function update(SavePostRequest $request, Post $postid){


        $postid->update([$request->validated()]);

        return to_route('posts.show', $postid)->with('status','Post update');
    }

    public function destroy(Post $postid){

        $postid->delete();

        return to_route('posts.index')->with('status', 'Post Delete');

    }
}